//
//  TestModel.swift
//  apmdemo
//
//  Created by Nagarro on 03/02/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

struct TestModel: Codable {
    
    let base, endAt, startAt: String
    let rates: [String: [String: Double]]
    
    enum CodingKeys: String, CodingKey {
        case base
        case endAt = "end_at"
        case startAt = "start_at"
        case rates
    }
}
