//
//  FirstViewController.swift
//  apmdemo
//
//  Created by Rupender on 30/01/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit
import FirebasePerformance

class FirstViewController: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var trace: Trace?
    var testModel: TestModel?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    func populateData() {
        
        self.headerLabel.text = "\(testModel?.base ?? ""), \(testModel?.startAt ?? "") - \(testModel?.endAt ?? "")"
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        trace = Performance.startTrace(name: "Dummy Trace")
        
        let urlString = URL(string: "https://appperformance.azurewebsites.net/api/HttpTrigger1?code=aovLmF8PJFjzWb5BsC05sm6nY0e5U8zQAod6amp/Vjhi/tYWlLACIA==")
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!)
                } else {
                    self.trace?.incrementMetric("Data Received", by: 1)
                    if let usableData = data {
                        let decoder = JSONDecoder()
                        self.testModel = try? decoder.decode(TestModel.self, from: usableData)
                        DispatchQueue.main.async {
                            self.populateData()
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        trace?.stop()
    }
}

extension FirstViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if let model = self.testModel {
            return model.rates.values.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let model = self.testModel {
            return Array(model.rates.keys)[section]
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let model = self.testModel {
            return Array(model.rates.values)[section].count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if let model = self.testModel {
            let itemArr = Array(model.rates.values)[indexPath.section]
            let itemDict = Array(itemArr)[indexPath.row]
            cell.textLabel?.text = "\(itemDict.key) : \(itemDict.value)"
        }
        return cell
    }
}
