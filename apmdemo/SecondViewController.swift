//
//  SecondViewController.swift
//  apmdemo
//
//  Created by Rupender on 30/01/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit
import FirebasePerformance

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.monitorSpecificRequest()
    }
    
    func monitorSpecificRequest() {
        guard let metric = HTTPMetric(url: URL(string: "https://stats.nba.com/stats/commonplayerinfo")!, httpMethod: .get) else { return }
        
        metric.start()
        guard let url = URL(string: "https://stats.nba.com/stats/commonplayerinfo") else { return }
        let request: URLRequest = URLRequest(url:url)
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { (urlData, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                metric.responseCode = httpResponse.statusCode
            }
            metric.stop()
        }
        dataTask.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

